import React from "react";
import "./ClassStopwatch.css"
import { Button } from "@material-ui/core"
import AccessTimeIcon from '@material-ui/icons/AccessTime';

export interface Props {

}

export interface State {
    active: boolean;
    runningTime: number;
}

class ClassStopwatch extends React.Component<Props, State> {
    timerInterval: any
    constructor(props: Props) {
        super(props);
        this.state = {
            active: false,
            runningTime: 0,
        }
        this.handleStart = this.handleStart.bind(this);
        this.handleClear = this.handleClear.bind(this);
    }

    componentWillUnmount() {
        clearInterval(this.timerInterval)
    }

    handleStart = () => {

        this.timerInterval = setInterval(
            () => {
                this.setState((state) => (
                    {
                        runningTime: state.runningTime + 1,
                    }
                ))
            }, 1000)
        this.setState({
            active: true,

        })
    }

    handleClear = () => {
        clearInterval(this.timerInterval)
        this.setState((state) => (
            {
                active: false,
                runningTime: 0,
            }
        ))
    }


    formattedTime = (time: number) => {
        let h: string | number = Math.floor(time / 3600);
        let m: string | number = Math.floor(time % 3600 / 60);
        let s: string | number = Math.floor(time % 3600 % 60);

        h = h > 0 ? h : 0
        m = m > 0 ? m : 0
        s = s > 0 ? s : 0

        h = (h < 10 ? "0" : "") + h
        m = (m < 10 ? "0" : "") + m
        s = (s < 10 ? "0" : "") + s

        return { h, m, s }
    }


    render() {
        const { active, runningTime } = this.state;
        const { h, m, s } = this.formattedTime(runningTime)
        const formattedTime = h + ":" + m + ":" + s
        return (
            <div className="stopwatch">
                <div className="header">React Stopwatch  <AccessTimeIcon /> </div>
                <div className="watch">
                    <div className="watch-timer">
                        {formattedTime}
                    </div>

                    <div className="control">
                        <Button variant="outlined" color="primary" onClick={this.handleStart}>
                            Start
                        </Button>
                        <Button variant="outlined" color="primary" onClick={this.handleClear}>
                            Clear
                        </Button>
                    </div>


                </div>

            </div>
        );
    }
}

export default ClassStopwatch;
