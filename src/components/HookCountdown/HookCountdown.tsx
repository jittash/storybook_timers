import React, { useState, useEffect } from "react";
import "./HookCountdown.css"

export interface Props {
    startTime: number;
}

const HookCountdown: React.FC<Props> = ({ startTime }) => {
    const [active, setactive] = useState<boolean>(true)
    const [remainingTime, setremainingTime] = useState<number>(startTime)
    const [day, setday] = useState<string>('')
    const [hour, sethour] = useState<string>('')
    const [min, setmin] = useState<string>('')
    const [sec, setsec] = useState<string>('')

    useEffect((): any => {
        let timerInterval: any;
        active ? timerInterval = setInterval(() => {
            setremainingTime(remainingTime => remainingTime - 1);
        }, 1000)
            :
            clearInterval(timerInterval);
        return () => clearInterval(timerInterval)
    }, [active])

    useEffect(() => {
        formattedTime(remainingTime)
    }, [remainingTime])

    const formattedTime = (time: number) => {
        let d: number | string = Math.floor(time / (3600 * 24))
        let h: string | number = Math.floor(time / 3600);
        let m: string | number = Math.floor(time % 3600 / 60);
        let s: string | number = Math.floor(time % 3600 % 60);

        /* Display 0 at one's place if number is smaller than 0*/

        d = d > 0 ? d : 0
        h = h > 0 ? h : 0
        m = m > 0 ? m : 0
        s = s > 0 ? s : 0

        /* Display 0 at ten's place if number is smaller than 10*/

        d = (d < 10 ? "0" : "") + d
        h = (h < 10 ? "0" : "") + h
        m = (m < 10 ? "0" : "") + m
        s = (s < 10 ? "0" : "") + s

        setday(d);
        sethour(h);
        setmin(m);
        setsec(s);
    }

    const handleClear = () => {
        setactive(false)
        setremainingTime(0)
    }
    return (
        <div className="countdown">
            <div className="head">
                <div className="head-text">Countdown Timer</div>
                <div className="head-control">
                    <button onClick={handleClear}>Clear</button>
                    <button>Settings</button>
                </div>
            </div>
            <div className="body">
                <div className="body-text">Countdown ends in...</div>
                <div className="body-timer">
                    <div className="timer-item">
                        <p className="single-text">{day}</p>
                        <p className="single-number">Days</p>
                    </div>
                    <div className="timer-item">
                        <p className="single-text">{hour}</p>
                        <p className="single-number">Hours</p>
                    </div>
                    <div className="timer-item">
                        <p className="single-text">{min}</p>
                        <p className="single-number">Mins</p>
                    </div>
                    <div className="timer-item">
                        <p className="single-text">{sec}</p>
                        <p className="single-number">Secs</p>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default HookCountdown;
