import React from "react";
import "./ClassCountdown.css";

export interface Props {
    startTime: number;
}

export interface State {
    active: boolean;
    remainingTime: number;
}

class ClassCountdown extends React.Component<Props, State> {

    timer: any
    constructor(props: Props) {
        super(props);
        this.state = {
            remainingTime: props.startTime,
            active: true,
        }
    }

    componentDidMount() {
        this.timer = setInterval(() => {
            this.decrementTime();
        }, 1000)
    }

    /* Prompt when countdown is over */

    componentDidUpdate(prevProps: Props, prevState: State) {
        if (this.state.remainingTime === 0) {
            alert("Time Up")
        }
    }
    componentWillUnmount() {
        clearInterval(this.timer)
    }

    decrementTime = () => {
        this.setState((state) => ({
            remainingTime: state.remainingTime - 1
        }))
    }

    handleClear = () => {
        clearInterval(this.timer)
        this.setState({
            active: false,
            remainingTime: 0,
        })
    }

    formattedTime = (time: number) => {
        let d: number | string = Math.floor(time / (3600 * 24))
        let h: number | string = Math.floor(time / 3600);
        let m: number | string = Math.floor(time % 3600 / 60);
        let s: number | string = Math.floor(time % 3600 % 60);

        /* Display 0 at one's place if number is smaller than 0*/

        d = d > 0 ? d : 0
        h = h > 0 ? h : 0
        m = m > 0 ? m : 0
        s = s > 0 ? s : 0

        /* Display 0 at ten's place if number is smaller than 10*/

        d = (d < 10 ? "0" : "") + d
        h = (h < 10 ? "0" : "") + h
        m = (m < 10 ? "0" : "") + m
        s = (s < 10 ? "0" : "") + s

        return { d, h, m, s }
    }

    render() {
        const { d, h, m, s } = this.formattedTime(this.state.remainingTime)
        return (
            <div className="countdown">
                <div className="head">
                    <div className="head-text">Countdown Timer</div>
                    <div className="head-control">
                        <button onClick={this.handleClear}>Clear</button>
                        <button>Settings</button>
                    </div>
                </div>
                <div className="body">
                    <div className="body-text">Countdown ends in...</div>
                    <div className="body-timer">
                        <div className="timer-item">
                            <p className="single-text">{d}</p>
                            <p className="single-number">Days</p>
                        </div>
                        <div className="timer-item">
                            <p className="single-text">{h}</p>
                            <p className="single-number">Hours</p>
                        </div>
                        <div className="timer-item">
                            <p className="single-text">{m}</p>
                            <p className="single-number">Mins</p>
                        </div>
                        <div className="timer-item">
                            <p className="single-text">{s}</p>
                            <p className="single-number">Secs</p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ClassCountdown;
