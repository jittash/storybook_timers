import React, { useState, useEffect } from "react";
import "./HookStopwatch.css";
import { Button } from "@material-ui/core";
import AccessTimeIcon from '@material-ui/icons/AccessTime';


export interface Props {

}

const HookStopwatch: React.FC<Props> = () => {
    const [active, setactive] = useState<boolean>(false)
    const [runningTime, setrunningTime] = useState<number>(0)

    useEffect((): any => {
        let timerInterval: any;
        active ? timerInterval = setInterval(() => {
            setrunningTime(runningTime => runningTime + 1)
        }, 1000)
            :
            clearInterval(timerInterval)
        return () => clearInterval(timerInterval)
    }, [active])

    const formattedTime = (time: number): string => {
        let h: string | number = Math.floor(time / 3600);
        let m: string | number = Math.floor(time % 3600 / 60);
        let s: string | number = Math.floor(time % 3600 % 60);

        h = h > 0 ? h : 0
        m = m > 0 ? m : 0
        s = s > 0 ? s : 0

        h = (h < 10 ? "0" : "") + h
        m = (m < 10 ? "0" : "") + m
        s = (s < 10 ? "0" : "") + s

        return `${h} : ${m} : ${s}`
    }

    const handleStart = () => {
        setactive(true);
    }

    const handleClear = () => {
        setactive(false)
        setrunningTime(0)
    }

    return (
        <div className="stopwatch">
            <div className="header">React Stopwatch <AccessTimeIcon /> </div>
            <div className="watch">
                <div className="watch-timer">
                    {formattedTime(runningTime)}
                </div>

                <div className="control">
                    <Button variant="outlined" color="primary" onClick={handleStart}>
                        Start
                    </Button>
                    <Button variant="outlined" color="primary" onClick={handleClear}>
                        Clear
                    </Button>
                </div>
            </div>
        </div>
    );
}

export default HookStopwatch;
